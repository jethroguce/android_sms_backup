package sms.jethro.com.sms

import android.database.Cursor
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import java.util.Date
import java.io.File
import java.io.FileWriter
import java.text.SimpleDateFormat

class MainActivity : AppCompatActivity() {

    internal lateinit var loadButton: Button
    internal val TAG = MainActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.i(TAG, "onCreate called")

        loadButton = findViewById<Button>(R.id.load_button)

        loadButton.setOnClickListener { view ->
            // hard coded for specific thread
            // you can find thread id using loadConversations()
            loadThread(10)
        }
    }

    private fun loadConversations() {
        Log.i(TAG, "Loading Conversations")
        val uriSMSURI: Uri? = Uri.parse("content://sms/conversations")
        val cursor: Cursor = contentResolver.query(uriSMSURI,null, null, null, null)

        cursor.moveToFirst()
        (1 .. cursor.count).map {
            val threadId = cursor.getString(1)
            val msgCount = cursor.getString(0)
            val snippet = cursor.getString(2)
            Log.i(TAG, "$threadId ($msgCount): $snippet")
            cursor.moveToNext()
        }
        Log.i(TAG, "Finished Loading Conversations")
    }

    private fun loadThread(threadId: Int) {
        Log.i(TAG, "Loading Thread: $threadId")

        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val dir = this.externalCacheDir
        val file = File(dir,"conversations.csv")
        file.createNewFile()
        Log.i(TAG, "Creating file at $dir")

        val threadUri: Uri? = Uri.parse("content://sms/")
        val cursor: Cursor = contentResolver.query(threadUri, null, "thread_id=$threadId", null, null)
        cursor.moveToFirst()

        val fileWriter = FileWriter(file)
        (1.. cursor.count).map {
            var id = cursor.getString(0)
            var ts: Long = cursor.getLong(4)
            var dateCreated: Date = Date(ts)
            var date = format.format(dateCreated)
            var snt = cursor.getLong(5)
            var dateSent = Date(snt)
            var dateSentString = format.format(dateSent)
            var type = cursor.getString(9)
            var content = cursor.getString(12)
            content = content.replace("\n", "[]")
            content = content.replace(",", "|")
            var dataString = "$id,$date,$dateSentString,$type,$content"
            Log.i(TAG, "Writing: $dataString")
            fileWriter.append("$dataString\n")
            cursor.moveToNext()
        }
        fileWriter.close()
        Log.i(TAG, "Finished Loading Thread: $threadId")
    }


}

